import React from "react";

const ButtonComponent = ({ fetchJokes }) => {
  return (
    <button
      className="btn btn-primary"
      onClick={this.fetchJokes("https://api.icndb.com/jokes/random/10")}
    >
      Wanna hear a joke?
    </button>
  );
};

export default ButtonComponent;
