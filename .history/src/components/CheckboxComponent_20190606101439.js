import React, { Component } from "react";
import App from "../App";

class CheckboxComponent extends App {
  constructor(props) {
    super(props);

    this.state = {
      isChecked: false
    };
  }

  render() {
    let text;

    if (this.state.isChecked) {
      text = "checked";
    } else {
      text = "unchecked";
    }

    return (
      <div className="form-check">
        <input
          name="isChecked"
          type="checkbox"
          id="defaultCheck1"
          className="form-check-input"
          checked={this.state.isChecked}
          onChange={this.toggleCheckbox}
        />

        <label className="form-check-label" htmlFor="defaultCheck1">
          {text}
        </label>
      </div>
    );
  }
}

export default CheckboxComponent;
