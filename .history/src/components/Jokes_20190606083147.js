import React from "react";

const Favorites = ({ jokes }) => {
  const JokesList = jokes.length
    ? jokes.map(joke => {
        return (
          <li className="collection-item" key={joke.id}>
            <p>{joke.joke}</p>

            <button className="btn btn-default">Klik hier</button>
          </li>
        );
      })
    : null;
  return <ul className="collection">{JokesList}</ul>;
};

export default Favorites;
