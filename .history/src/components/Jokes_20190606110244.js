import React from "react";

const Jokes = ({ jokes, addToFavorites }) => {
  const JokesList = jokes.length
    ? jokes.map(joke => {
        return (
          <li className="list-group-item" key={joke.id}>
            <div className="row">
              <p className="col s9">{joke.joke}</p>
              <div className="col s2 offset-s1">
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={() => {
                    addToFavorites(joke, joke.id);
                  }}
                >
                  <i className="far fa-star" />
                </button>
              </div>
            </div>
          </li>
        );
      })
    : null;
  return <ul className="list-group">{JokesList}</ul>;
};

export default Jokes;
