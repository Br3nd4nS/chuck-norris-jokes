import React, { Component } from "react";
import "./App.scss";
import JokesList from "./components/Jokes";
import Favorites from "./components/Favorites";

const maxJokes = 10;

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      jokes: [],
      favorites: []
    };
  }

  fetchJokes = async url => {
    if (this.state.jokes.length <= maxJokes - 1) {
      try {
        let response = await fetch(url);
        let data = await response.json();
        let sum = this.state.jokes.length + data.value.length;

        if (!(sum > maxJokes)) {
          this.setState({
            jokes: [...this.state.jokes, ...data.value]
          });
        } else {
          alert(`Je kan niet meer dan ${maxJokes} hebben in de lijst, Grapjas`);
        }
      } catch (error) {
        console.log(error);
      }
    } else {
      return null;
    }
  };

  render() {
    return (
      <div className="App">
        <main role="main">
          <h1>React app</h1>

          <div className="row justify-content-between">
            <div className="col-sm-5">
              <JokesList
                jokes={this.state.jokes}
                addToFavorites={this.addToFavorites}
              />
            </div>
            <div className="col-sm-5" />
          </div>

          <div className="row justify-content-between">
            <div className="col-sm-2">
              <button
                className="btn btn-default"
                onClick={() => {
                  this.fetchJokes("https://api.icndb.com/jokes/random/10");
                }}
              >
                Wanna hear a joke?
              </button>
            </div>
          </div>
        </main>
      </div>
    );
  }
}

export default App;
