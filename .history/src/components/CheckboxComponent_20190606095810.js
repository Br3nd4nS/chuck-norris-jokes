import React, { Component } from "react";

class CheckboxComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isChecked: false
    };
  }

  render() {
    let txt;

    if (this.state.isChecked) {
      txt = "checked";
    } else {
      txt = "unchecked";
    }

    return (
      <div className="form-check">
        <input
          name="isChecked"
          type="checkbox"
          id="defaultCheck1"
          className="form-check-input"
          checked={this.state.isChecked}
          onChange={this.toggleCheckbox}
        />

        <label className="form-check-label" htmlFor="defaultCheck1">
          {this.props.text}
        </label>
      </div>
    );
  }
}

export default CheckboxComponent;
