import React, { Component } from "react";

const CheckboxComponent = ({ text, toggleCheckbox }) => {
  return (
    <div className="form-check">
      <input
        name="isChecked"
        type="checkbox"
        id="defaultCheck1"
        className="form-check-input"
        checked={text}
        onChange={toggleCheckbox}
      />

      <label className="form-check-label" htmlFor="defaultCheck1">
        {text.text}
      </label>
    </div>
  );
};

export default CheckboxComponent;
