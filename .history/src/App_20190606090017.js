import React, { Component } from "react";
import "./App.scss";
import JokesList from "./components/Jokes";
import Favorites from "./components/Favorites";
import FavoriteJokes from "./components/Favorites";

const maxJokes = 10;

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      jokes: [],
      favorites: []
    };
  }

  fetchJokes = async url => {
    if (this.state.jokes.length <= maxJokes - 1) {
      try {
        let response = await fetch(url);
        let data = await response.json();
        let sum = this.state.jokes.length + data.value.length;

        if (!(sum > maxJokes)) {
          this.setState({
            jokes: [...this.state.jokes, ...data.value]
          });
        } else {
          alert(`Je kan niet meer dan ${maxJokes} hebben in de lijst, Grapjas`);
        }
      } catch (error) {
        console.log(error);
      }
    } else {
      return null;
    }
  };

  addToFavorites = (joke, id) => {
    if (this.state.favorites.length <= maxJokes - 1) {
      // Filter the joke out of the jokesList
      const jokes = this.state.jokes.filter(joke => {
        return joke.id !== id;
      });

      // Add the new joke to the favorites list and update the state
      this.setState({ favorites: [...this.state.favorites, joke], jokes });
    } else {
      alert(`Je kan niet meer dan ${maxJokes} hebben in de lijst, Grapjas`);
    }
  };

  render() {
    return (
      <div className="App">
        <main className="container" role="main">
          <div className="row justify-content-between">
            <div className="col-sm-5">
              <JokesList
                jokes={this.state.jokes}
                addToFavorites={this.addToFavorites}
              />
            </div>
            <div className="col-sm-5">
              <FavoriteJokes
                favorites={this.state.favorites}
                deleteFromFavorites={this.deleteFromFavorites}
              />
            </div>
          </div>

          <div className="row justify-content-between">
            <div className="col-sm-2">
              <button
                className="btn btn-default"
                onClick={() => {
                  this.fetchJokes("https://api.icndb.com/jokes/random/10");
                }}
              >
                Wanna hear a joke?
              </button>
            </div>
          </div>
        </main>
      </div>
    );
  }
}

export default App;
