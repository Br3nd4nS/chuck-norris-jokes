import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import ButtonComponent from "./components/ButtonComponent";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

test("Fake Test", () => {
  expect(true).toBeTruthy();
});

const add = (x, y) => {
  return x + y;
};

test("add", () => {
  const value = add(1, 2);
  expect(value);
});
