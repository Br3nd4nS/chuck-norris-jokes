import React, { Component } from "react";

// class ButtonComponent extends Component {
//   render() {
//     return (
//       <button
//         className="btn btn-primary"
//         onClick={() => {
//           this.fetchJokes("https://api.icndb.com/jokes/random/10");
//         }}
//       >
//         Wanna hear a joke?
//       </button>
//     );
//   }
// }

const ButtonComponent = ({ fetchJokes }) => {
  return (
    <button
      className="btn btn-primary"
      onClick={() => {
        this.fetchJokes("https://api.icndb.com/jokes/random/10");
      }}
    >
      Wanna hear a joke?
    </button>
  );
};

export default ButtonComponent;
