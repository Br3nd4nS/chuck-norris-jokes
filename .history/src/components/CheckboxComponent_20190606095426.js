import React from "react";

class CheckboxComponent {
  constructor(props) {
    this.state = {
      jokes: [],
      favorites: [],
      isChecked: false
    };
  }

  render() {
    return (
      <div className="form-check">
        <input
          name="isChecked"
          type="checkbox"
          id="defaultCheck1"
          className="form-check-input"
          checked={this.state.isChecked}
          onChange={this.toggleCheckbox}
        />

        <label className="form-check-label" htmlFor="defaultCheck1">
          {txt}
        </label>
      </div>
    );
  }
}
