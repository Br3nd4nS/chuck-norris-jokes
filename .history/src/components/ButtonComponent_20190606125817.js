import React from "react";

const ButtonComponent = ({ fetchJokes }) => {
  return (
    <button
      disabled={this.state.jokes.length <= 10 ? "disabled" : null}
      className="btn btn-primary"
      onClick={() => {
        fetchJokes("https://api.icndb.com/jokes/random/10");
      }}
    >
      Wanna hear some jokes?
    </button>
  );
};

export default ButtonComponent;
