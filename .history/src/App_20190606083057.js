import React, { Component } from "react";
import "./App.scss";
import Jokes from "./components/Jokes";
import Favorites from "./components/Favorites";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      jokes: [],
      favorites: []
    };
  }

  render() {
    return (
      <div className="App">
        <main role="main">
          <h1>React app</h1>

          <div className="row justify-content-between">
            <div className="col-sm-5">
              <Jokes />
            </div>
            <div className="col-sm-5">
              <Favorites />
            </div>
          </div>

          <div className="row justify-content-between">
            <div className="col-sm-2">
              <button className="btn btn-default">Wanna hear a joke?</button>
            </div>
          </div>
        </main>
      </div>
    );
  }
}

export default App;
