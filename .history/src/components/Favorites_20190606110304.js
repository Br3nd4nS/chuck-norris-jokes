import React from "react";

const FavoriteJokes = ({ favorites, deleteFromFavorites }) => {
  const FavoritesList = favorites.length
    ? favorites.map(joke => {
        return (
          <li className="list-group-item" key={joke.id}>
            <div className="row">
              <p className="col s9">{joke.joke}</p>
              <div className="col s2 offset-s1">
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={() => {
                    deleteFromFavorites(joke.id);
                  }}
                >
                  <i className="fas fa-times" />
                </button>
              </div>
            </div>
          </li>
        );
      })
    : null;
  return <ul className="list-group">{FavoritesList}</ul>;
};

export default FavoriteJokes;
