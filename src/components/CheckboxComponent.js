import React from "react";

const CheckboxComponent = ({ checked, toggleCheckbox }) => {
  let text;

  if (checked) {
    text = "Nieuwe grap aub";
  } else {
    text = "Geen nieuwe grap meer";
  }

  return (
    <form>
      <div className="form-check">
        <input
          name="isChecked"
          type="checkbox"
          id="defaultCheck1"
          className="form-check-input"
          checked={checked}
          onChange={toggleCheckbox}
        />

        <label className="form-check-label" htmlFor="defaultCheck1">
          {text}
        </label>
      </div>
    </form>
  );
};

export default CheckboxComponent;
