import React from "react";

const ButtonComponent = ({ maxJokes, fetchJokes }) => {
  return (
    <button
      disabled={maxJokes.length}
      className="btn btn-primary"
      onClick={() => {
        fetchJokes("https://api.icndb.com/jokes/random/10");
      }}
    >
      Wanna hear some jokes?
    </button>
  );
};

export default ButtonComponent;
