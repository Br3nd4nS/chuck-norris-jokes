import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import ButtonComponent from "./components/ButtonComponent";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

const add = (x, y) => {
  return x + y;
};

test("add", () => {
  expect(add(1, 2)).toBe(3);
});

test("test 10 new jokes", () => {
  expect();
});
