import React from "react";

const Jokes = ({ jokes, addToFavorites }) => {
  const JokesList = jokes.length
    ? jokes.map(joke => {
        return (
          <li className="list-group-item" key={joke.id}>
            <p>{joke.joke}</p>

            <button
              className="btn btn-default material-icons dp48"
              onClick={() => {
                addToFavorites(joke, joke.id);
              }}
            />
          </li>
        );
      })
    : null;
  return <ul className="list-group">{JokesList}</ul>;
};

export default Jokes;
