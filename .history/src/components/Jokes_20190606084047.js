import React from "react";

const Jokes = ({ jokes, addToFavorites }) => {
  const JokesList = jokes.length
    ? jokes.map(joke => {
        return (
          <li className="list-group-item" key={joke.id}>
            <div className="row">
              <p className="col-sm-8">{joke.joke}</p>
              <button
                type="button"
                className="btn btn-primary col-sm-3 col-sm-offset-1"
                onClick={() => {
                  addToFavorites(joke, joke.id);
                }}
              >
                Wanna hear a joke?
              </button>
            </div>
          </li>
        );
      })
    : null;
  return <ul className="list-group">{JokesList}</ul>;
};

export default Jokes;
