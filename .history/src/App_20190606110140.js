import React, { Component } from "react";
import "./App.scss";
import JokesList from "./components/Jokes";
import FavoriteJokes from "./components/Favorites";
import ButtonComponent from "./components/ButtonComponent";

const maxJokes = 10;

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      jokes: [],
      favorites: [],
      isChecked: false
    };
  }

  toggleCheckbox = event => {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });

    if (this.state.isChecked === false) {
      this.timerFunction();
    } else {
      return;
    }
  };

  timerFunction = () => {
    if (this.state.jokes.length < maxJokes) {
      setInterval(() => {
        this.fetchJokes("https://api.icndb.com/jokes/random/1");
      }, 5000);
    } else {
    }
  };

  // Asynchrone functie om de data op te halen
  fetchJokes = async url => {
    if (this.state.jokes.length <= maxJokes - 1) {
      try {
        let response = await fetch(url);
        let data = await response.json();
        let sum = this.state.jokes.length + data.value.length;

        if (!(sum > maxJokes)) {
          this.setState({
            jokes: [...this.state.jokes, ...data.value]
          });
        } else {
          alert(`Je kan niet meer dan ${maxJokes} hebben in de lijst, Grapjas`);
        }
      } catch (error) {
        console.log(error);
      }
    } else {
      return null;
    }
  };

  addToFavorites = (joke, id) => {
    if (this.state.favorites.length <= maxJokes - 1) {
      // Filter the joke out of the jokesList
      const jokes = this.state.jokes.filter(joke => {
        return joke.id !== id;
      });

      // Add the new joke to the favorites list and update the state
      this.setState({ favorites: [...this.state.favorites, joke], jokes });
    } else {
      alert(`Je kan niet meer dan ${maxJokes} hebben in de lijst, Grapjas`);
    }
  };

  deleteFromFavorites = id => {
    // Filter de 'joke' uit de favorietenlijst
    const favorites = this.state.favorites.filter(joke => {
      return joke.id !== id;
    });
    this.setState({ favorites });
  };

  componentDidMount() {
    this.fetchLocalStorageHistory();

    // add event listener to save state to localStorage
    // when user leaves/refreshes the page
    window.addEventListener(
      "beforeunload",
      this.saveStateToLocalStorage.bind(this)
    );

    console.log(
      "On component did mount, is checkbox checked? " +
        (this.state.isChecked === true)
    );
  }

  componentWillUnmount() {
    window.removeEventListener(
      "beforeunload",
      this.saveStateToLocalStorage.bind(this)
    );

    // saves if component has a chance to unmount
    this.saveStateToLocalStorage();
  }

  fetchLocalStorageHistory() {
    // for all items in state
    for (let key in this.state) {
      if (key === "favorites") {
        // if the key exists in localStorage
        if (localStorage.hasOwnProperty(key)) {
          // get the key's value from localStorage
          let value = localStorage.getItem(key);

          // parse the localStorage string and setState
          try {
            value = JSON.parse(value);
            this.setState({ [key]: value });
          } catch (e) {
            // handle empty string
            this.setState({ [key]: value });
          }
        }
      }
    }
  }

  saveStateToLocalStorage() {
    // for every item in React state
    for (let key in this.state) {
      // save to localStorage
      if (key === "favorites") {
        localStorage.setItem(key, JSON.stringify(this.state[key]));
      }
    }
  }

  render() {
    let txt;

    if (this.state.isChecked) {
      txt = "checked";
    } else {
      txt = "unchecked";
    }

    return (
      <div className="App">
        <main className="container" role="main">
          <div className="row">
            <div className="col s6">
              <JokesList
                jokes={this.state.jokes}
                addToFavorites={this.addToFavorites}
              />
            </div>
            <div className="col s5">
              <FavoriteJokes
                favorites={this.state.favorites}
                deleteFromFavorites={this.deleteFromFavorites}
              />
            </div>
          </div>

          <div className="row">
            <div className="col s12 mg-t10">
              <ButtonComponent fetchJokes={this.fetchJokes} />
            </div>

            <div className="col s12 mg-t10">
              <label>
                <input
                  name="isChecked"
                  type="checkbox"
                  id="defaultCheck"
                  className="filled-in"
                  checked={this.state.isChecked}
                  onChange={this.toggleCheckbox}
                />
                <span className="form-check-label">{txt}</span>
              </label>
            </div>
          </div>
        </main>
      </div>
    );
  }
}

export default App;
